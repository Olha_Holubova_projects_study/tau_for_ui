package stepdefinitions;

import action.BBCNewsPageAction;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import manager.PageFactoryManager;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import sharedcontext.Context;
import sharedcontext.TestContext;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class BBCNewsPageSteps {

    WebDriver driver;
    PageFactoryManager pageFactoryManager;
    BBCNewsPageAction newsPageAction;
    TestContext testContext;

    public BBCNewsPageSteps(@NotNull TestContext testContext) {
        driver = testContext.getDriver();
        pageFactoryManager = testContext.getPageFactoryManager();
        newsPageAction = testContext.getNewsPageAction();
        this.testContext = testContext;
    }

    @Then("the titles of the promo articles on then news page are")
    public void theTitlesOfThePromoArticlesOnThenNewsPageAre(List<String> listOfTitles) {
        assertEquals(listOfTitles, newsPageAction.promoTitles());
    }

    @Then("the title of the headline article is")
    public void theNameOfTheHeadlineArticleIs(String articleTitle) {
        assertEquals(testContext.getNewsPageAction().getHeadlineTitleText(), articleTitle);
    }

    @And("the user stores the text of the Category link of the headline article")
    public void theUserStoresTheTextOfTheCategoryLinkOfTheHeadlineArticle() {
        testContext.scenarioContext.setContext(Context.CATEGORY, newsPageAction.getHeadlineCategoryLinkText());
    }

}
