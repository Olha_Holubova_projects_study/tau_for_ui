package stepdefinitions;

import action.Navigate;
import action.BBCNewsPageAction;
import dataProviders.ConfigFileReader;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import sharedcontext.TestContext;

public class NavigateSteps {
    WebDriver driver;
    PageFactoryManager pageFactoryManager;
    Navigate navigate;
    ConfigFileReader configFileReader;
    BBCNewsPageAction newsPageAction;

    public NavigateSteps(@NotNull TestContext testContext) {
        driver = testContext.getDriver();
        pageFactoryManager = testContext.getPageFactoryManager();
        navigate = testContext.getNavigate();
        configFileReader = new ConfigFileReader();
    }

    @Before
    public void testsSetup() {
        newsPageAction = new BBCNewsPageAction(pageFactoryManager);
    }

    @After
    public void tearDown() {
    }

    @When("the user switches to {string} language using language link")
    public void theUserTranslateToLanguageUsingLanguageLink(String language) {
        navigate.translateTo(language);
    }

    @Given("a web browser is at the {string} home page")
    public void openTheHomePage(String url) {
        navigate.toHomepage(configFileReader.getApplicationUrl(url));
    }

    @When("the user navigates on {string} page")
    public void theUserNavigatesOnPageByName(String itemName) {
        navigate.clickTopMenuByName(itemName);
    }

    @And("the user navigates on Coronavirus: Send us your questions on Coronavirus tab")
    public void theUserNavigatesOnTab() {
        if (driver.getCurrentUrl().contains("news")) {
            newsPageAction.closeRegisterAlert();
        }
        navigate.clickPageMenuByName("Coronavirus");
        navigate.clickNewsTabMenuByName("Your Coronavirus Stories");
        newsPageAction.questionTitle();
    }
}
