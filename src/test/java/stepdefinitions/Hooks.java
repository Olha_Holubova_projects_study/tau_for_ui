package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import sharedcontext.TestContext;

public class Hooks {
    TestContext testContext;

    public Hooks(TestContext context) {
        testContext = context;
    }

    @Before
    public void testsSetup() {
    }

    @After
    public void tearDown() {
        testContext.getDriver().quit();
    }
}
