package stepdefinitions;

import action.LIFeedResultAction;
import action.QuestionFormAction;
import fillform.Form;
import fillform.GenerateTestDate;
import dataProviders.ConfigFileReader;
import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import sharedcontext.TestContext;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BBCQuestionFormSteps {

    WebDriver driver;
    PageFactoryManager pageFactoryManager;
    QuestionFormAction questionFormAction;
    Form form;
    LIHomePageSteps homePageSteps;
    ConfigFileReader configFileReader;
    TestContext testContext;

    Map<String, String> items = new HashMap();

    public BBCQuestionFormSteps(@NotNull TestContext testContext) {
        driver = testContext.getDriver();
        pageFactoryManager = testContext.getPageFactoryManager();
        questionFormAction = new QuestionFormAction(pageFactoryManager);
        form = new Form(pageFactoryManager);
        this.testContext = testContext;
    }

    @When("the user click the Submit button if empty {string} field")
    public void theUserEntersSavedButtonEmptyFields(String fieldName) {
        if (!fieldName.equals("All")) {
            form.fillForm(items, fieldName);
        }
        questionFormAction.setTermsOfServiceCheckboxValue(true);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(100));
        questionFormAction.clickSubmitButton();
    }

    @Then("the error messages display")
    public void theErrorMessagesDisplays(List<String> listOfErrors) {
        List<String> errors = questionFormAction.getFormErrorMessagesText();
        assertEquals(errors, listOfErrors);
    }

    @Then("the error message displays")
    public void theErrorMessageDisplays(List<String> listOfErrors) {
        List<String> errors = questionFormAction.getFormErrorMessagesText();
        assert (errors.contains(listOfErrors.get(0)));
    }

    private String generateData() {
        String url = "lorem_ipsum";
        String str;
        homePageSteps = new LIHomePageSteps(testContext);
        configFileReader = new ConfigFileReader();
        homePageSteps.navigate.toHomepage(configFileReader.getApplicationUrl(url));
        homePageSteps.radioButtonAction.setValue("words");
        homePageSteps.homeAction.enterNumberToNumberField(1);
        homePageSteps.homeAction.clickGenerateButton();
        LIFeedResultAction feedResult = homePageSteps.feedResult;
        str = feedResult.getValue();
        return str;
    }

    @Before("@GeneratedData")
    public void BeforeStep() {
        GenerateTestDate testDate = new GenerateTestDate(generateData());
        items = testDate.generateTestData();
    }
}