package stepdefinitions;

import action.BBCSearchPageAction;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import sharedcontext.Context;
import sharedcontext.TestContext;

import static org.junit.Assert.assertEquals;

public class BBCSearchResultPage {

    WebDriver driver;
    PageFactoryManager pageFactoryManager;
    BBCSearchPageAction searchPageAction;
    TestContext testContext;

    public BBCSearchResultPage(@NotNull TestContext testContext) {
        driver = testContext.getDriver();
        pageFactoryManager = testContext.getPageFactoryManager();
        searchPageAction = testContext.getSearchPageAction();
        this.testContext = testContext;
    }

    @When("the user enters saved text in the Search bar")
    public void theUserEntersSavedTextInTheSearchBar() {
        searchPageAction.getSearch(testContext.scenarioContext.getContext(Context.CATEGORY).toString());
    }

    @Then("the name of the first article is")
    public void theNameOfTheFirstArticleIs(String firstTitle) {
        assertEquals(firstTitle, searchPageAction.getSearchResultByNumber(0).getText());
    }
}