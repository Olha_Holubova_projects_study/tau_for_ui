package stepdefinitions;

import action.LIFeedResultAction;
import io.cucumber.java.en.Then;
import manager.PageFactoryManager;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import sharedcontext.Context;
import sharedcontext.TestContext;

import static org.junit.Assert.*;

public class LIFeedResultSteps {

    WebDriver driver;
    PageFactoryManager pageFactoryManager;
    LIFeedResultAction feedResult;
    TestContext testContext;

    private static final long DEFAULT_NUMBER = 5;

    public LIFeedResultSteps(@NotNull TestContext testContext) {
        driver = testContext.getDriver();
        pageFactoryManager = testContext.getPageFactoryManager();
        feedResult = testContext.getFeedResult();
        this.testContext = testContext;
    }

    @Then("the first paragraph on the feed page starts with {string}")
    public void theFirstParagraphOnTheFeedPageStartsWithKeySentence(String keySentence) {
        assertTrue(feedResult.checkStringStartWith(keySentence));
    }

    @Then("the amount of {string} is {long} on the Feed page")
    public void theAmountOfElementsIsSizeOnTheFeedPage(String nameCheckbox, long number) {
        if (number<=0) number = DEFAULT_NUMBER;
        assertEquals(feedResult.amountOf(nameCheckbox),number);
    }

    @Then("generated text on the feed page does not start with {string}")
    public void generatedTextOnTheFeedPageDoesNotStartWithLoremIpsum(String keySentence) {
        assertFalse(feedResult.checkStringStartWith(keySentence));
    }

    @Then("the average occurrence of the word in all generated paragraphs is between {int} and {int}")
    public void theAverageOccurrenceOfTheWordInAllGeneratedParagraphsIsBetweenAnd(int min, int max) {
        int averageTotal = (Integer) testContext.scenarioContext.getContext(Context.COUNT);
        assertTrue(((averageTotal >= min) && (averageTotal <= max)));
    }
}