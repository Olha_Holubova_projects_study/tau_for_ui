package stepdefinitions;

import action.LIFeedResultAction;
import action.LIHomeAction;
import action.Navigate;
import action.LIRadioButtonAction;
import dataProviders.ConfigFileReader;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import sharedcontext.Context;
import sharedcontext.TestContext;

import static org.junit.Assert.assertTrue;

public class LIHomePageSteps {

    WebDriver driver;
    PageFactoryManager pageFactoryManager;
    LIHomeAction homeAction;
    LIFeedResultAction feedResult;
    TestContext testContext;
    Navigate navigate;
    LIRadioButtonAction radioButtonAction;
    ConfigFileReader configFileReader;

    public LIHomePageSteps(@NotNull TestContext testContext) {
        driver = testContext.getDriver();
        pageFactoryManager = testContext.getPageFactoryManager();
        homeAction = testContext.getHomeAction();
        feedResult = testContext.getFeedResult();
        this.testContext = testContext;
        navigate = testContext.getNavigate();
        radioButtonAction = testContext.getRadioButton();
        configFileReader= new ConfigFileReader();
    }

    @When("the user presses Generate Lorem Ipsum button")
    public void theUserPressesGenerateLoremIpsum() {
        homeAction.clickGenerateButton();
    }

    @Then("the first paragraph contains the word {string}")
    public void theFirstParagraphContainsTheWord(String keyword) {
        assertTrue(homeAction.containsKeyWordTextWhatIsLoremIpsum(keyword));
    }

    @When("And the user Generate Lorem Ipsum with {long} {string}")
    public void andTheUserGenerateLoremIpsumWithSizeElements(long number, String nameCheckbox) {
        homeAction.enterNumberToNumberField(number);
        radioButtonAction.setValue(nameCheckbox);
        homeAction.clickGenerateButton();
    }

    @When("the user generates text with unchecked Start with Lorem Ipsum checkbox")
    public void theUserGeneratesTextWithUncheckedStartWithLoremIpsumCheckbox() {
        homeAction.uncheckStartWithCheckBox();
        homeAction.clickGenerateButton();
    }

    @When("the user generates text {int} times with default settings and get the average number of paragraphs containing the word {string}")
    public void theUserGeneratesTextTimesWithDefaultSettings(int count, String keyword) {
        int countParagraph = 0;
        String url = "lorem_ipsum";
        for (int i=0; i<count; i++){
            homeAction.clickGenerateButton();
            countParagraph += feedResult.countLoremIpsumParagraphs(keyword);
            navigate.toHomepage(configFileReader.getApplicationUrl(url));
        }
        testContext.scenarioContext.setContext(Context.COUNT, countParagraph/count);
    }

}
