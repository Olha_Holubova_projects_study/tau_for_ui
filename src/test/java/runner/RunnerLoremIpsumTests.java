package runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/main/resources/smokeLoremIpsum.feature",
        glue = "stepdefinitions",
        plugin = {"pretty", "html:reports/Reports/LoremIpsum.html"},
        monochrome = true
)
public class RunnerLoremIpsumTests {
}
