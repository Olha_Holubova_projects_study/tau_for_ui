Feature: Smoke
  As a user
  I want to test some main site functionality
  So that I can be sure that site works correctly

  Scenario: The word "рыба" is displayed correctly in the first paragraph ("Что такое Lorem Ipsum?")
    Given a web browser is at the 'lorem_ipsum' home page
    When the user switches to 'Pyccкий' language using language link
    Then the first paragraph contains the word 'рыба'

  Scenario: The default setting result in text starting with Lorem ipsum
    Given a web browser is at the 'lorem_ipsum' home page
    When the user presses Generate Lorem Ipsum button
    Then the first paragraph on the feed page starts with 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'

  Scenario Outline: Lorem Ipsum is generated with correct size
    Given a web browser is at the 'lorem_ipsum' home page
    When And the user Generate Lorem Ipsum with <size> '<elements>'
    Then the amount of '<elements>' is <size> on the Feed page

    Examples:
      |elements|size|
      |word    |10  |
      |word    |-1  |
      |word    |0   |
      |word    |5   |
      |word    |20  |
      |byte    |10  |
      |byte    |-1  |
      |byte    |0   |
      |byte    |5   |
      |byte    |20  |

  Scenario: Uncheck "Start with Lorem Ipsum" checkbox
    Given a web browser is at the 'lorem_ipsum' home page
    When the user generates text with unchecked Start with Lorem Ipsum checkbox
    Then generated text on the feed page does not start with 'Lorem Ipsum'

  Scenario: Randomly generated text paragraphs contain the word "lorem" with probability of more than 40%
    Given a web browser is at the 'lorem_ipsum' home page
    When the user generates text 10 times with default settings and get the average number of paragraphs containing the word "lorem"
    Then the average occurrence of the word in all generated paragraphs is between 2 and 3