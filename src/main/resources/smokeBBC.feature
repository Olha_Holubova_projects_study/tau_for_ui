Feature: SmokeBBC
  As a user
  I want to test some main site functionality
  So that I can be sure that site works correctly

  Scenario: the title of headline article on News page
    Given a web browser is at the 'bbc' home page
    When the user navigates on 'News' page
    Then the title of the headline article is
      |New Australia leader signals climate policy change|

  Scenario: the promo articles are displayed on the News page
    Given a web browser is at the 'bbc' home page
    When the user navigates on 'News' page
    Then the titles of the promo articles on then news page are
      | 'It shouldn't be controversial here'  |
      | Meet Australia's new prime minister   |
      |Scott Morrison runs out of miracles    |

  Scenario: The search by the news category name displays the given article
    Given a web browser is at the 'bbc' home page
    And the user navigates on 'News' page
    And the user stores the text of the Category link of the headline article
    When the user navigates on 'Search' page
    When the user enters saved text in the Search bar
    Then the name of the first article is
      | Australia with Simon Reeve |

  Scenario: The question to BBC form doesn't send with empty required fields
    Given a web browser is at the 'bbc' home page
    And the user navigates on 'News' page
    And the user navigates on Coronavirus: Send us your questions on Coronavirus tab
    When the user click the Submit button if empty 'All' field
    Then the error messages display
      |can't be blank               |
      |Name can't be blank          |
      |Email address can't be blank |

  @GeneratedData
  Scenario: The question to BBC form did not send with empty question data
    Given a web browser is at the 'bbc' home page
    And the user navigates on 'News' page
    And the user navigates on Coronavirus: Send us your questions on Coronavirus tab
    When the user click the Submit button if empty 'question' field
    Then the error message displays
      |can't be blank |

  @GeneratedData
  Scenario: The question to BBC form did not send with empty name data
    Given a web browser is at the 'bbc' home page
    And the user navigates on 'News' page
    And the user navigates on Coronavirus: Send us your questions on Coronavirus tab
    When the user click the Submit button if empty 'Name' field
    Then the error message displays
      |Name can't be blank  |