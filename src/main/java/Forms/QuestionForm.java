package Forms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

import java.util.List;

public class QuestionForm extends BasePage {

    @FindBy(xpath = "//div[@class='embed-content-container']")
    private WebElement questionForm;

    public QuestionForm(WebDriver driver) {
        super(driver);
    }

    public WebElement getFormElement(String placeholder) {
        return questionForm.findElement(By.cssSelector("*[placeholder*='" + placeholder + "']"));
    }

    public WebElement getTermsOfServiceCheckbox() {
        return questionForm.findElement(By.xpath(".//input[@type='checkbox']"));
    }

    public WebElement getSubmitButton() {
        return questionForm.findElement(By.xpath(".//button[@class='button']"));
    }

    public List<WebElement> getFormErrorMessages() {
        return questionForm.findElements(By.xpath(".//div[@class='input-error-message']"));
    }
}
