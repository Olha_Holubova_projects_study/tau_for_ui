package pages.loremIpsum;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class LIHomePage extends BasePage {

    @FindBy(xpath = "//h2[contains(text(), 'Lorem Ipsum')]/following-sibling::p")
    private WebElement textWhatIsLoremIpsum;

    @FindBy(xpath = "//input[@id = 'generate']")
    private WebElement generateButton;

    @FindBy(xpath = "//input[@id='amount']")
    private WebElement numberField;

    @FindBy(xpath = "//input[@name = 'start']")
    private WebElement startWithCheckBox;

    public LIHomePage(WebDriver driver) {
        super(driver);
    }

    public WebElement getTextWhatIsLoremIpsum(){
        return textWhatIsLoremIpsum;
    }

    public WebElement getGenerateButton(){
        return generateButton;
    }

    public WebElement getNumberField(){
        return numberField;
    }

    public WebElement getStartWithCheckBox(){
        return startWithCheckBox;
    }

    public WebElement getCheckBoxByName(String name){
        return this.driver.findElement(By.xpath("//td/label[contains(text(), '"+name+"')]"));
    }
}
