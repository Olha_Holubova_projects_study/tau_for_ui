package pages.loremIpsum;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

import java.util.List;

public class LIFeedPage extends BasePage {

    @FindBy(xpath = "//div[@id='lipsum']/descendant::p[1]")
    private WebElement firstParagraph;

    @FindBy(xpath = "//div[@id='lipsum']/p")
    private static List<WebElement> loremParagraphs;

    public LIFeedPage(WebDriver driver) {
        super(driver);
    }

    public String getFirstParagraphText(){
        return firstParagraph.getText();
    }

    public List<WebElement> getLoremParagraphsList(){
        return loremParagraphs;
    }
}
