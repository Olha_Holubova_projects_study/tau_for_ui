package pages.bbc;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

import java.util.List;

public class BBCNewsPage extends BasePage {

    @FindBy(xpath = "//div[contains(@class,'top-stories__primary-item')]//h3[contains(@class,'heading__title')]")
    private WebElement headlineTitle;

    @FindBy(xpath = "//div[@data-entityid='container-top-stories#1']//a[contains(@class, 'section-link')]/span")
    private WebElement headlineCategory;

    @FindBy(xpath = "//div[contains(@class,'nw-c-5-slice')]//h3")
    private List<WebElement> promoArticles;

    @FindBy(xpath = "//li[contains(@class, 'nw-c-related-story')]//span[@class = 'nw-o-link-split__text gs-u-align-bottom']")
    private List<WebElement> promoHeadArticles;

    @FindBy(xpath = "//button[@aria-label='Close']")
    private WebElement closeRegistrationButton;

    @FindBy(xpath = "//h3[contains(text(),'Your questions')]/..")
    private WebElement questionTitle;

    public BBCNewsPage(WebDriver driver) {
        super(driver);
    }
    public WebElement getHeadlineTitle() {
        return headlineTitle;
    }

    public WebElement getHeadlineCategoryLink() {
        return headlineCategory;
    }

    public List<WebElement> getPromoArticles() {
        return promoArticles;
   }

    public List<WebElement> getPromoHeadArticles() {
        return promoHeadArticles;
    }

    public WebElement getCloseRegistrationButton() {
        return closeRegistrationButton;
    }

    public WebElement getQuestionTitle() {
        return questionTitle;
    }

}