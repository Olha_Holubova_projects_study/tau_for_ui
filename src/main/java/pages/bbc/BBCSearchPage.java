package pages.bbc;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

import java.util.List;

public class BBCSearchPage extends BasePage {

    @FindBy(xpath = "//input[@id='search-input']")
    private WebElement searchBar;

    @FindBy(xpath = "//p[contains(@class,'PromoHeadline')]/span")
    private List<WebElement> searchResultTitles;

    public BBCSearchPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getSearchBar() {
        return searchBar;
    }

    public List<WebElement> getSearchResultsTitles() {
        return  searchResultTitles;
    }

}
