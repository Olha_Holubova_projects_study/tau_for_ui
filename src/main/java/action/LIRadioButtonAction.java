package action;

import manager.PageFactoryManager;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import pages.loremIpsum.LIHomePage;

public class LIRadioButtonAction {

    WebDriver driver;
    LIHomePage homePage;
    PageFactoryManager pageFactoryManager;

    public LIRadioButtonAction(WebDriver driver, @NotNull PageFactoryManager pageFactoryManager) {
        this.driver = driver;
        this.pageFactoryManager = pageFactoryManager;
        homePage = pageFactoryManager.getHomePage();
    }

    public void setValue(String nameRadioButton) {
        homePage.getCheckBoxByName(nameRadioButton).click();
    }

}
