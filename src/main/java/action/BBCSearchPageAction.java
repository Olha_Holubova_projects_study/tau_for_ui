package action;

import manager.PageFactoryManager;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.bbc.BBCSearchPage;

public class BBCSearchPageAction {

    WebDriver driver;
    BBCSearchPage searchPage;
    PageFactoryManager pageFactoryManager;

    public BBCSearchPageAction(WebDriver driver, @NotNull PageFactoryManager pageFactoryManager) {
        this.driver = driver;
        this.pageFactoryManager = pageFactoryManager;
        searchPage = pageFactoryManager.getSearchPage();
    }

    public void getSearch(String query) {
        searchPage = pageFactoryManager.getSearchPage();
        searchPage.getSearchBar().clear();
        searchPage.getSearchBar().sendKeys(query);
        searchPage.getSearchBar().sendKeys(Keys.ENTER);
    }

    public WebElement getSearchResultByNumber(int count) {
        searchPage.waitVisibilityOfElement(300, searchPage.getSearchBar());
        return searchPage.getSearchResultsTitles().get(count);
    }

}
