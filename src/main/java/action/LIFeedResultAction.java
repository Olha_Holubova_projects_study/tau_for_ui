package action;

import manager.PageFactoryManager;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.loremIpsum.LIFeedPage;

import java.util.StringTokenizer;

public class LIFeedResultAction {
    WebDriver driver;
    LIFeedPage feedPage;
    PageFactoryManager pageFactoryManager;

    public LIFeedResultAction(WebDriver driver, @NotNull PageFactoryManager pageFactoryManager) {
        this.driver = driver;
        this.pageFactoryManager = pageFactoryManager;
        feedPage = pageFactoryManager.getFeedPage();
    }

    public boolean checkStringStartWith(String keySentence) {
        return feedPage.getFirstParagraphText().startsWith(keySentence);
    }

    private static long countWords(String str) {
        if (str == null || str.isEmpty())
            return 0;

        StringTokenizer tokens = new
                StringTokenizer(str);

        return tokens.countTokens();
    }

    private static long countLetters(String str) {
        if (str == null || str.isEmpty()) {
            return 0;
        }

        return str.length();
    }

    public long amountOf(String checkbox) {
        switch (checkbox) {
            case "word":
                return countWords(feedPage.getFirstParagraphText());
            case "byte":
                return countLetters(feedPage.getFirstParagraphText());
            default:
                return 0;
        }
    }

    public int countLoremIpsumParagraphs(String keyword) {
        return (int) feedPage.getLoremParagraphsList().stream()
                .map(WebElement::getText)
                .filter(elem -> elem.contains(keyword))
                .count();
    }

    public String getValue() {
        return feedPage.getFirstParagraphText();
    }
}
