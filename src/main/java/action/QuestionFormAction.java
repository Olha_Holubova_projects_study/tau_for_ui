package action;

import Forms.QuestionForm;
import manager.PageFactoryManager;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class QuestionFormAction {
    QuestionForm questionForm;
    PageFactoryManager pageFactoryManager;

    public QuestionFormAction(@NotNull PageFactoryManager pageFactoryManager) {
        this.pageFactoryManager = pageFactoryManager;
        questionForm = pageFactoryManager.getQuestionForm();
    }

    public void clickSubmitButton() {
        questionForm.getSubmitButton().click();
    }

    public List<String> getFormErrorMessagesText() {
        return questionForm.getFormErrorMessages().stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public void setTermsOfServiceCheckboxValue(boolean setValue) {
        if (setValue) {
            if (!questionForm.getTermsOfServiceCheckbox().isSelected()) {
                questionForm.getTermsOfServiceCheckbox().click();
            }
        } else {
            if (questionForm.getTermsOfServiceCheckbox().isSelected()) {
                questionForm.getTermsOfServiceCheckbox().click();
            }
        }
    }
}