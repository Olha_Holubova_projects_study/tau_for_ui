package action;

import manager.PageFactoryManager;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.bbc.BBCNewsPage;

import java.util.List;
import java.util.stream.Collectors;

public class BBCNewsPageAction {

    WebDriver driver;
    BBCNewsPage newsPage;
    PageFactoryManager pageFactoryManager;

    public BBCNewsPageAction(@NotNull PageFactoryManager pageFactoryManager, WebDriver driver) {
        this.driver = driver;
        this.pageFactoryManager = pageFactoryManager;
        newsPage = pageFactoryManager.getNewsPage();
    }

    public BBCNewsPageAction(PageFactoryManager pageFactoryManager) {
        this.pageFactoryManager = pageFactoryManager;
        newsPage = pageFactoryManager.getNewsPage();
    }

    public String getHeadlineTitleText() {
        newsPage.waitForPageLoadComplete(400);
        return newsPage.getHeadlineTitle().getText();
    }

    public List<String> promoTitles() {
        return newsPage.getPromoHeadArticles().stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public void questionTitle() {
        newsPage.getQuestionTitle().click();
    }

    public String getHeadlineCategoryLinkText() {
        return newsPage.getHeadlineCategoryLink().getText();
    }

    public void closeRegisterAlert() {
        newsPage.waitVisibilityOfElement(30, newsPage.getCloseRegistrationButton());
        newsPage.getCloseRegistrationButton().click();
    }
}