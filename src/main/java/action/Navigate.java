package action;

import manager.PageFactoryManager;
import menu.LILanguageMenu;
import menu.BBCPageMenu;
import menu.BBCTopMenu;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import pages.bbc.BBCNewsPage;
import pages.loremIpsum.LIHomePage;

public class Navigate {

    WebDriver driver;
    LIHomePage homePage;
    LILanguageMenu languageMenu;
    PageFactoryManager pageFactoryManager;
    BBCTopMenu topMenu;
    BBCPageMenu pageMenu;
    BBCNewsPage newsPage;

    public Navigate(WebDriver driver, @NotNull PageFactoryManager pageFactoryManager) {
        this.driver = driver;
        this.pageFactoryManager = pageFactoryManager;
        homePage = pageFactoryManager.getHomePage();
        languageMenu = pageFactoryManager.getLanguageMenu();
        topMenu = pageFactoryManager.getTopMenu();
        pageMenu = pageFactoryManager.getPageMenu();
        newsPage = pageFactoryManager.getNewsPage();
    }

    public Navigate(PageFactoryManager pageFactoryManager) {
        this.pageFactoryManager = pageFactoryManager;
        homePage = pageFactoryManager.getHomePage();
    }

    public void toHomepage(String url) {
        driver.get(url);
    }

    public void translateTo(String languageName) {
        homePage.waitForPageLoadComplete(300);
        languageMenu.getLanguageLink(languageName).click();
    }

    public void clickTopMenuByName(String itemName) {
        topMenu.getTopMenuItemByName(itemName).click();
    }

    public void clickPageMenuByName(String itemName) {
        pageMenu.getPageMenuItemByName(itemName).click();
    }

    public void clickNewsTabMenuByName(String itemName) {
        pageMenu.getNewsPageMenuItemByName(itemName).click();
    }

}
