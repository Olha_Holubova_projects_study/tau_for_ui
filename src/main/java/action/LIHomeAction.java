package action;

import manager.PageFactoryManager;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.loremIpsum.LIHomePage;

public class LIHomeAction {

    WebDriver driver;
    LIHomePage homePage;
    PageFactoryManager pageFactoryManager;

    public LIHomeAction(WebDriver driver, @NotNull PageFactoryManager pageFactoryManager) {
        this.driver = driver;
        this.pageFactoryManager = pageFactoryManager;
        homePage = pageFactoryManager.getHomePage();
    }

    public void clickGenerateButton() {
        homePage.waitForPageLoadComplete(500);
        homePage.getGenerateButton().click();
    }

    public boolean containsKeyWordTextWhatIsLoremIpsum(String keyword) {
        return homePage.getTextWhatIsLoremIpsum().getText().contains(keyword);
    }

    public void enterNumberToNumberField(long number) {
        homePage.getNumberField().clear();
        homePage.getNumberField().sendKeys(Long.toString(number));
    }

    public void uncheckStartWithCheckBox() {
        WebElement startCheckBox = homePage.getStartWithCheckBox();
        if (startCheckBox.isSelected())
            homePage.getStartWithCheckBox().click();
    }
}
