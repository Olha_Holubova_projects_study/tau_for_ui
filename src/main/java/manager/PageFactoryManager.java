package manager;

import Forms.QuestionForm;
import menu.LILanguageMenu;
import menu.BBCPageMenu;
import menu.BBCTopMenu;
import org.openqa.selenium.WebDriver;
import pages.bbc.BBCNewsPage;
import pages.bbc.BBCSearchPage;
import pages.loremIpsum.LIFeedPage;
import pages.loremIpsum.LIHomePage;

public class PageFactoryManager {

    private WebDriver driver;
    private LIHomePage homePage;
    private LIFeedPage feedPage;
    private LILanguageMenu languageMenu;
    private BBCNewsPage newsPage;
    private BBCSearchPage searchPage;
    private BBCTopMenu topMenu;
    private BBCPageMenu pageMenu;
    private QuestionForm questionForm;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public LIHomePage getHomePage() {
        return (homePage == null) ? homePage = new LIHomePage(driver) : homePage;
    }

    public LIFeedPage getFeedPage() {
        return (feedPage == null) ? feedPage = new LIFeedPage(driver) : feedPage;
    }

    public LILanguageMenu getLanguageMenu() {
        return (languageMenu == null) ? languageMenu = new LILanguageMenu(driver) : languageMenu;
    }

    public BBCNewsPage getNewsPage() {
        return (this.newsPage == null) ? this.newsPage = new BBCNewsPage(driver) : this.newsPage;
    }

    public BBCSearchPage getSearchPage() {
        return (this.searchPage == null) ? this.searchPage = new BBCSearchPage(driver) : this.searchPage;
    }

    public BBCTopMenu getTopMenu() {
        return (topMenu == null) ? topMenu = new BBCTopMenu(driver) : topMenu;
    }

    public BBCPageMenu getPageMenu() {
        return (pageMenu == null) ? pageMenu = new BBCPageMenu(driver) : pageMenu;
    }

    public QuestionForm getQuestionForm() {
        return (questionForm == null) ? new QuestionForm(driver) : questionForm;
    }



}