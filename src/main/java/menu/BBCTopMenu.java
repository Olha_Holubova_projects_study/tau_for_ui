package menu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class BBCTopMenu extends BasePage {

    @FindBy(xpath = "//div[@id='orb-header']")
    private WebElement topMenu;


    public BBCTopMenu(WebDriver driver) {
        super(driver);
    }

    public WebElement getTopMenuItemByName(String menuItem) {
        return topMenu.findElement(By.partialLinkText(menuItem));
    }
}
