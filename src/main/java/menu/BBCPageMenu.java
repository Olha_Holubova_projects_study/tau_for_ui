package menu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class BBCPageMenu extends BasePage {
    public BBCPageMenu(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//nav[contains(@class, 'nw-c-nav__wide')]")
    private WebElement pageMenu;

    @FindBy(xpath = "//nav[contains(@class, 'nw-c-nav__wide-secondary')]")
    private WebElement tabMenu;

    public WebElement getPageMenuItemByName(String menuItem) {
        return pageMenu.findElement(By.partialLinkText(menuItem));
    }

    public WebElement getNewsPageMenuItemByName(String menuItem) {
        return tabMenu.findElement(By.partialLinkText(menuItem));
    }

}
