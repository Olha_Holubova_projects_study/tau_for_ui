package menu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class LILanguageMenu extends BasePage {

    public LILanguageMenu(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@id = 'Languages']")
    private WebElement languageMenu;

    public WebElement getLanguageLink(String name){
        return languageMenu.findElement(By.xpath("//a[text() = '" + name + "']"));
    }
}
