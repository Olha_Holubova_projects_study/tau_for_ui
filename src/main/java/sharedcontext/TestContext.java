package sharedcontext;

import action.*;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class TestContext {

    private WebDriver driver;
    private PageFactoryManager pageFactoryManager;
    private Navigate navigate;
    private LIFeedResultAction feedResult;
    private LIHomeAction homeAction;
    public ScenarioContext scenarioContext;
    public LIRadioButtonAction radioButton;
    public BBCNewsPageAction newsPageAction;
    public BBCSearchPageAction searchPageAction;

    public TestContext() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
        navigate = new Navigate(driver, pageFactoryManager);
        feedResult = new LIFeedResultAction(driver, pageFactoryManager);
        homeAction = new LIHomeAction(driver, pageFactoryManager);
        scenarioContext = new ScenarioContext();
        radioButton = new LIRadioButtonAction(driver, pageFactoryManager);
        newsPageAction = new BBCNewsPageAction(pageFactoryManager,driver);
        searchPageAction = new BBCSearchPageAction(driver, pageFactoryManager);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public PageFactoryManager getPageFactoryManager() {
        return pageFactoryManager;
    }

    public Navigate getNavigate() {
        return navigate;
    }

    public LIFeedResultAction getFeedResult() {
        return feedResult;
    }

    public LIHomeAction getHomeAction() {
        return homeAction;
    }

    public LIRadioButtonAction getRadioButton() {
        return radioButton;
    }

    public BBCNewsPageAction getNewsPageAction() { return newsPageAction;}
    public BBCSearchPageAction getSearchPageAction() { return searchPageAction;}
}
