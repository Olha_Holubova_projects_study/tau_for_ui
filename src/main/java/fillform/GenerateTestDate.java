package fillform;

import java.util.HashMap;
import java.util.Map;

public class GenerateTestDate {

    Map<String, String> items = new HashMap();
    String item;

    public GenerateTestDate(String item) {
        this.item = item;
    }

    public Map<String, String> generateTestData() {
        items.put("question", item);
        items.put("Name", item);
        items.put("Email", item + "@gmail.com");
        items.put("number", "123456890");
        items.put("Location", item);
        items.put("Age", "17");

        return items;
    }

}
