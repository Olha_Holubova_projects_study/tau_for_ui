package fillform;

import Forms.QuestionForm;
import manager.PageFactoryManager;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class Form {
    PageFactoryManager pageFactoryManager;
    QuestionForm questionForm;

    public Form(@NotNull PageFactoryManager pageFactoryManager) {
        this.pageFactoryManager = pageFactoryManager;
        questionForm = pageFactoryManager.getQuestionForm();
    }

    public void fillForm(Map<String, String> values, String fieldName) {
        for (String key : values.keySet()) {
            if (key.equals(fieldName)) {
                questionForm.getFormElement(key).sendKeys("");
            } else {
                questionForm.getFormElement(key).sendKeys(values.get(key));
            }
        }
    }
}
